﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Добавление сотрудника
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<EmployeeResponse>> CreateAsync(EmployeeRequest employee)
        {
            if (employee is null)
                return BadRequest();

            try
            {
                var employeeModel = new Employee()
                {
                    Email = employee.Email,
                    FirstName = employee.FirstName,
                    LastName = employee.LastName
                };

                var result = await _employeeRepository.Create(employeeModel);

                return new EmployeeResponse()
                {
                    Id = result.Id,
                    Email = result.Email,
                    FullName = employeeModel.FullName,
                    AppliedPromocodesCount = result.AppliedPromocodesCount
                };
            }
            catch (ArgumentNullException ex)
            {
                return BadRequest(ex.Message);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        /// <summary>
        /// Редактирование сотрудника
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPatch]
        public async Task<ActionResult<EmployeeResponse>> UpdateAsync(EmployeeRequest employee)
        {
            if (employee is null || employee.Id is null)
                return BadRequest();

            try
            {
                var employeeModel = new Employee()
                {
                    Id = employee.Id.Value,
                    Email = employee.Email,
                    FirstName = employee.FirstName,
                    LastName = employee.LastName
                };

                var result = await _employeeRepository.Update(employeeModel);

                return new EmployeeResponse()
                {
                    Id = result.Id,
                    Email = result.Email,
                    FullName = employeeModel.FullName,
                    AppliedPromocodesCount = result.AppliedPromocodesCount
                };
            }
            catch (ArgumentNullException ex)
            {
                return BadRequest(ex.Message);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        /// <summary>
        /// Удаление сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("id:guid")]
        public async Task<ActionResult<bool>> DeleteAsync(Guid id)
        {
            var existEmployee = this._employeeRepository.GetByIdAsync(id);
            if (existEmployee is null)
                return BadRequest($"Employee with id = {id} not found");

            return await _employeeRepository.Delete(id);
        }
    }
}