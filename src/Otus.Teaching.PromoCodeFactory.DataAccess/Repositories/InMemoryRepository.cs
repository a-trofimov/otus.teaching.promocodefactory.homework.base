﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }

        public Task<List<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> Create(T entity)
        {
            if (entity is null)
                throw new ArgumentNullException(nameof(entity));

            entity.Id = Guid.NewGuid();
            Task.FromResult(Data.Append(entity));

            return Task.FromResult(entity);
        }

        public Task<T> Update(T entity)
        {
            if (entity is null)
                throw new ArgumentNullException(nameof(entity));

            var editEntityIndex = Data.FindIndex(x => x.Id == entity.Id);
            if (editEntityIndex < 0)
                throw new ArgumentNullException($"Not foud employee with id = {entity.Id}");

            Data[editEntityIndex] = entity;

            return Task.FromResult(entity);
        }

        public Task<bool> Delete(Guid id)
        {
            var exist = Data.FirstOrDefault(x => x.Id == id);
            if (exist is null)
                throw new ArgumentNullException(nameof(exist));

            Data.Remove(exist);

            return Task.FromResult(true);
        }
    }
}